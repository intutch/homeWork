const mysql = require('mysql2')

class Database {
    constructor(host, user, password, database) {
        this.instance = mysql.createConnection({
            host: host,
            user: user,
            password: password,
            database: database
        })
    }
    getConnection() {
        return new Promise((resolve, reject) => {
            let sql = 'SELECT * FROM user'
            this.instance.query(sql, (err, results) => {
                if (err) console.log(err)
                else resolve(results)
            })
        })
    }

}

module.exports = () => new Database('localhost', 'root', '', 'codecamp')