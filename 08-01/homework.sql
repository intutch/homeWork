create TABLE bill( ISNB_book int, id_employee int, book_price int, Volume int, create_at timestamp default now() )

create TABLE employees (
    id int auto_increment, 
    firstname varchar(32),
    lastname varchar(32),
    age int,
    create_at timestamp DEFAULT now(),
    PRIMARY key (id)
  )

  
insert into employees (firstname, lastname, age) values
     ('Philippe', 'Coutinho', 25),
     ('Diego', 'Costa', 30),
     ('Sergej', 'Milinkovic-Savic', 22),
     ('Emre', 'Can', 23),
     ('Rob', 'Holding', 22),
     ('David', 'Lyiz', 30),
     ('Ben','Brereton', 18),
     ('Kylian', 'Mbappé', 19),
     ('Marcus','Rashford', 19),
     ('Tom', 'Davies', 19),
     ('Shiori', 'Aratani', 18)
      ;  

       delete from employees
      where id = 5;

    alter table employees
      add column address varchar(225)
      ;
     select count(*) from employees

      select * from employees
      where age < 20 ;

     create table book(
    ISNB int auto_increment,
     name varchar(225),
     price int,
     create_at timestamp default now(),
     PRIMARY key (ISNB)
 )

 insert into book (name, price) values 
     ('1984', 199),
     ('theStranger', 299),
      ('FireandFury', 300),
      ('utopia', 500),
      ('SocialContract', 300),
      ('ThePrince', 200),
      ('TheLittlePrince', 100),
      ('DasKapital', 200),
      ('WarandPeace', 800),
      ('AnnaKarenina', 200)
 ;

select * from book  where name like '%Fire%';

 select * from book  where name like '%a%' limit 4;

 insert into bill ( ISNB_book, id_employee, book_price, Volume ) values
 (10,4,200,1),
 (1,10,199,1),
 (4,2,500,1),
 (8,8,200,1),
 (9,10,800,1),
 (2,1,299,1),
 (10,1,200,1),
 (3,2,300,1),
 (3,7,300,1),
 (4,8,500,2),
 (9,9,800,8),
 (7,4,300,3),
 (5,8,300,1),
 (8, 8, 400,2),
 (4,4,500,3),
 (3,2,300,1),
 (3, 7,300,1),
 (4,8,500,2),
 (9,9,800,8),
 (7,4,300,3);

select sum(Volume) from bill;

select sum(Volume*book_price) from bill;

 select distinct ISNB_book from bill;