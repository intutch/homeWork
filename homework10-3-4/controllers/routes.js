const {
    db
} = require('../lib/database.js')




module.exports = (app) => {
    app.use(async(ctx, next) => {
        try {
            let results = await db.execute("SELECT * FROM user")
            await ctx.render('table', {
                "row": results
            })
            await next()
        } catch (err) {
            ctx.status = 400
            ctx.body = `OMG-Error ${err.message}`
            winLog.error(`${err.message}`)
        }
    })
}