import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import { Menu, Carousel, Card, Row, Col, Rate } from 'antd'
class App extends Component {
  render() {
    return (
      <div className="App">
       <div className="header">
 <Menu
   mode="horizontal"
   theme="dark"
 >
  <Menu.Item key="product">
    <h3 className = "text-h3">Product</h3>
  </Menu.Item>
  <Menu.Item key="About">
    <h3 className = "text-h3">About</h3>
  </Menu.Item>
 </Menu>
</div>
<Carousel autoplay>
  <div><h3>1</h3></div>
  <div><h3>2</h3></div>
  <div><h3>3</h3></div>
  <div><h3>4</h3></div>
</Carousel>

<Row gutter={16}>
<Col span={6}>
            <Card>
              <div className="custom-image">
                <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
              </div>
              <div className="custom-card">
                <h3>Europe Street beat</h3>
                <p>www.instagram.com</p>
                <p><Rate /></p>
              </div>
            </Card>
          </Col>
          <Col span={6}>
            <Card>
              <div className="custom-image">
                <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
              </div>
              <div className="custom-card">
                <h3>Europe Street beat</h3>
                <p>www.instagram.com</p>
                <p><Rate /></p>
              </div>
            </Card>
          </Col>
          <Col span={6}>
            <Card>
              <div className="custom-image">
                <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
              </div>
              <div className="custom-card">
                <h3>Europe Street beat</h3>
                <p>www.instagram.com</p>
                <p><Rate /></p>
              </div>
            </Card>
          </Col>
          <Col span={6}>
            <Card>
              <div className="custom-image">
                <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
              </div>
              <div className="custom-card">
                <h3>Europe Street beat</h3>
                <p>www.instagram.com</p>
                <p><Rate /></p>
              </div>
            </Card>
          </Col>
</Row>
      </div>
    );
    
  }
}

export default App;
