import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import { Form, Icon, Input, Button } from 'antd';
const FormItem = Form.Item;

class NormalLoginForm  extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
  
  render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
    const userNameError = isFieldTouched('userName') && getFieldError('userName');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    return (
      
      <div className="App">
      <div class="split left">
      <div class="centered">
      <h2 class="color"> <Icon type="search" />ติดตามสิ่งที่คุณสนใจ</h2>
      <h2 class="color"> <Icon type="usergroup-add" />ฟังสิ่งที่ผู้คนกำลังพูดถึง</h2>
      <h2 class="color"> <Icon type="message" />เข้าร่วมบทสนทนา</h2>

  </div>
</div>

<div class="split right">
      <div> 
      <Form layout="inline" onSubmit={this.handleSubmit}>
        <FormItem
          validateStatus={userNameError ? 'error' : ''}
          help={userNameError || ''}
        >
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
          )}
        </FormItem>
        <FormItem
          validateStatus={passwordError ? 'error' : ''}
          help={passwordError || ''}
        >
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
            
          )}
          <a  className="left1"> ลืมรหัสผ่าน?</a>
        </FormItem>
        <FormItem>
          <Button
            htmlType="submit"
          >
            Log in
          </Button>
        </FormItem>
      </Form>
      </div>
  <div class="centered">
  <div class="size">
  <p class="m-0"><Icon type="twitter"  /></p>
  </div>
  <h1>ดูสิ่งที่เกิดขึ้นบนโลกในขณะนี้</h1>
  <h3>เข้าร่วมทวิตเตอร์วันนี้</h3>
  <Form onSubmit={this.handleSubmit} className="login-form">
  <FormItem>
    {getFieldDecorator('userName', {
      rules: [{ required: true, message: 'Please input your username!' }],
    })(
      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
    )}
  </FormItem>
  <FormItem>
    {getFieldDecorator('password', {
      rules: [{ required: true, message: 'Please input your Password!' }],
    })(
      <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
    )}
    
  </FormItem>
  <FormItem>
    <div class="left1" >
    <Button type="primary" htmlType="submit" >
      Log in
    </Button>
    </div>
    <div class="right1">
    <label> บัญชีผู้ใช้อยู่แล้ว? </label>
    <a href="" > เข้าสู่ระบบ</a>
    </div>
  </FormItem>
</Form>
  </div>
</div>
      </div>
    );
  }
}
const WrappedNormalLoginForm = Form.create()(NormalLoginForm);


export default WrappedNormalLoginForm;
