import Head from 'next/head'
export default () => (
  <div>
    <Head>
      <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/antd/2.9.3/antd.min.css' />
    </Head>
    <div className="header">Header</div>
    <div className="content">Content</div>
  </div>
)