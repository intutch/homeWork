select c.id, c.name as course_name, i.name as instructor_name
    from courses as c
    left join instructors as i on i.id = c.teach_by
    where i.id is null
    order by c.id;

    select c.id, c.name as course_name, i.name as instructor_name
    from courses as c
    right join instructors as i on i.id = c.teach_by
    where c.id is null
    order by c.id;


insert into student (name) values 
     ('Philippe'),
     ('Diego'),
     ('Milinkovic-Savic'),
     ('Emre'),
     ('Rob'),
     ('David'),
     ('Ben'),
     ('Kylian'),
     ('Marcus'),
     ('Tom'),
     ('Shiori');

insert into enrolls (student_id, course_id) values 
(1,1),
(2,10),
(3,25),
(4,11),
(5,12),
(6,10),
(7,8),
(8,20),
(9,6),
(10,15),
(11,24);

select distinct e.course_id,  c.name as courses 
from enrolls as e
inner join courses as c on c.id = e.course_id;

select e.course_id,  c.name as courses 
from enrolls as e 
right join courses as c on c.id = e.course_id
where e.course_id is null 
order by e.course_id


select distinct c.name as course_name , c.price as course_price , i.name as i_name
from enrolls as e
inner join courses as c on c.id = e.course_id
left join instructors as i on i.id = c.teach_by
where c.teach_by is not null
order by e.course_id;


-- SELECT  c.name as course_name, c.price as price, i.name as instructor, s.name as student_name FROM enrolls 
-- RIGHT join courses as c on c.id = enrolls.course_id
-- RIGHT JOIN instructors as i on i.id = c.teach_by
-- RIGHT JOIN students as s on s.id = enrolls.student_id
-- where e.course_id is null 
SELECT  c.name as course_name, c.price as price, i.name as instructor , c.id FROM enrolls 
RIGHT join courses as c on c.id = enrolls.course_id
left JOIN instructors as i on i.id = c.teach_by
where enrolls.course_id is null ;

SELECT  c.name as course_name, c.price as price, i.name as instructor FROM enrolls 
RIGHT join courses as c on c.id = enrolls.course_id
INNER JOIN instructors as i on i.id = c.teach_by
where enrolls.course_id is null ;



