import React, { Component } from 'react';
import {Input, Row, Col, List} from 'antd'
const Search = Input.Search;


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      term: '',
      item: [],
      isLoading: true
    }
  }

  
  render() {
    return (
      <div className="m-10">
       <Row>
      <Col span={8}></Col>
      <Col span={8}><Search placeholder="content" enterButton="Submit" size="large" />
      <List
      bordered
      dataSource={this.state.data}
      renderItem={item => (<List.Item>{item}</List.Item>)}
    />
      </Col>
      <Col span={8}></Col>
      </Row>
      
      </div>
    );
  }
}

export default App;
