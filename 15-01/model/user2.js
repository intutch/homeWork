function createEntity(row) {
    return {
        id: row.id,
        firstname: row.firstname,
        lastname: row.lastname
    }
}
async function find(db, id) {
    const [rows] = await db.execute(`
    select
    id,firstname, lastname
    from user
    where id = ?
    `, [id])
    return createEntity(rows[0])
}
async function findAll(db) {
    const [rows] = await db.execute(`
    select
    id,firstname, lastname
    from user
    `)
    return rows.map(createEntity)
}

async function store(db, user) {
    if (!user.id) {
        const result = await db.execute(`
    insert into user (
    firstname, lastname
    ) values (
    ?, ?
    )
    `, [user.firstname, user.lastname])
        user.id = result.insertId
        return
    }
    return db.execute(`
    update user
    set
    firstname = ?,
    lastname = ?
    where id = ?
    `, [user.firstname, user.lastname,
        user.id
    ])
}

async function findUserName(db, name) {
    const [rows] = await db.execute(`
    select
    id,firstname, lastname
    from user
    where firstname = ?
    `, [name])
    return createEntity(rows[0])
}

function remove(db, user) {
    return db.execute(`
    delete from user where id = ?
    `, [user.id])
}
module.exports = function (db) {
    return {
        find,
        findAll,
        store,
        findUserName,
        remove
    }
}