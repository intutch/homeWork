const mysql2 = require('mysql2/promise')
module.exports = pool = mysql2.createPool({
    host:'127.0.0.1',
    user:'root',
    database:'codecamp'
})