const mysql = require('mysql2/promise')

module.exports = (async (config) =>{
    const pool = mysql.createPool({
        host: '127.0.0.1',
        user: 'root',
        database: 'codecamp'
    })
    const db = await pool.getConnection()
    return db
} )