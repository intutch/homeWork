module.exports = {
    create,
    list,
    edit,
    remove
}

async function create(db, ISBN, title, price, imgUrl) {
    await db.execute(`insert into book (bookId, name, price, nameImg) values (?, ?, ?, ?)`, [ISBN, title, price, imgUrl])
}

function createEntity(row) {
    return {
        id: row.bookId,
        name: row.name,
        price: row.price,
        nameImg: row.nameImg
    }
}

async function list(db) {
    let [rows] = await db.execute(`select * from book`)
    return rows.map(createEntity)
}

async function edit(db, ISBN, title, price, imgUrl) {
    return await db.execute(`update book set name = ?, price = ?, nameImg = ? where bookId = ?`, [title, price, imgUrl, ISBN])
}

async function remove(db, id) {
    await db.execute(`delete from book where bookId = ?` ,[id])
}