import $ from 'jquery'
import {
    create,
    edit,
    remove2
} from './bookstore'

$('#addItem').on("click", function () {
    try {
        let url = "http://localhost:3200/add"
        let json = {
            ISBN: $("#ISBN").val(),
            title: $("#title").val(),
            price: $("#price").val(),
            imgUrl: $("#imgUrl").val()
        }
        console.log(json)
        let fetchData = {
            method: 'POST',
            body: JSON.stringify(json),
            headers: new Headers({
                'Content-type': 'application/json'
            })
        }
        fetch(url, fetchData)
            .then(res => {
                create(json.ISBN, json.title, json.price, json.imgUrl)
            })
    } catch (err) {
        alert(`Error: ${err}`)
    }
})

function setIDEdit(ID) {
    $("#Edit_ISBN").val(ID)
    console.log(ID)
}

window.setIDEdit = setIDEdit

$("body").on("click", "#editItem2", function () {
    
    try {
        let json = {
            ISBN: $("#Edit_ISBN").val(),
            title: $("#Edit_title").val(),
            price: $("#Edit_price").val(),
            imgUrl: $("#Edit_imgUrl").val()
        }
        console.log(json.ISBN)
        let url = `http://localhost:3200/edit/${json.ISBN}`
        console.log(url)
        let fetchData = {
            method: 'PATCH',
            body: JSON.stringify(json),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }
        fetch(url, fetchData)
            .then(res => {
                edit(json.ISBN, json.title, json.price, json.imgUrl)
            })
    } catch (err) {
        alert(`Error: ${err}`)
    }
})

$("body").on("click", "#delete", function (){
    try{
        let json = {ISBN: $(this).attr("bookId")}
        console.log(json.ISBN)
        let url = `http://localhost:3200/delete/${json.ISBN}`
        let fetchData = {
            method: 'DELETE',
            body: JSON.stringify(json),
            headers: new Headers({
                'content-Type': 'application/json'
            })
        }
        fetch(url, fetchData)
        .then(res => {
            remove2(json.ISBN)
        })
    }catch(err){
        alert(`Error: ${err}`)
    }
})
