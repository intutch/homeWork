const koa = require('koa')
const session = require('koa-session')


let sessionStore = {}


const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 10e3,
    httpOnly: true,
    store: {
        async get(key, maxAge, {rolling}) { //นับอายุ maxage ใหม่
            console.log(await find(key))
            return sessionStore[key]
        },
        async set(key, sess, maxAge, {rolling}) {
            let row = await find(key)
            if(!row[0]){
                await save(key, JSON.stringify(sess))
            }else{
                await updata(JSON.stringify(sess), key)
            }
            sessionStore[key] = sess
        },
        destroy(key) {
            delete sessionStore[key]
        }
    }
}

const app = new koa()

app.keys = ['supersecret']

app.use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)


async function handler(ctx,next) {
    let n = ctx.session.views || 0
    ctx.session.views = ++n
    ctx.body = `${n} views`
}

async function find(key){
    const dataBase = require('./lib/db')
    let db = await dataBase()
    let sql = `select * from sessions where sessionId = ? `
    let [rows] = await db.execute(sql,[key])
    await db.release()
    return rows
}

async function save(key, session){
    const dataBase = require('./lib/db')
    let db = await dataBase()
    const [result] = await db.execute(`insert into sessions (sessionId, sessionData) values(?,?)`,[key,session])
    result.insertId
    await db.release()
}

async function updata(session, key){
    const dataBase = require('./lib/db')
    let db = await dataBase()
    const [result] = await db.execute(`update sessions set sessionData = ? where sessionId = ?`,[session, key])
    await db.release()
}