const fs = require('fs')
//เนื่องจาก asyc อ่านยากและไม่สามารถกำหนดให้ใครทำงานก่อนหลังได้ จึงจำต้อง มี promise ที่ทำหน้าที่กำหนดว่าใครทำงานก่อนหลัง
let readHead = new Promise(function (resolve, rejuct) {
    fs.readFile('head.txt', 'utf8', function (err, dataHead) {
        if (err)
            reject(err)
        else
            resolve(dataHead + '\n')
    })
})
let readBody = new Promise(function (resolve, rejuct) {
    fs.readFile('body.txt', 'utf8', function (err, dataBody) {
        if (err)
            reject(err)
        else
            resolve(dataBody + '\n')
    })
})
let readLeg = new Promise(function (resolve, rejuct) {
    fs.readFile('leg.txt', 'utf8', function (err, dataLeg) {
        if (err)
            reject(err)
        else
            resolve(dataLeg + '\n')
    })
})
let readFeet = new Promise(function (resolve, rejuct) {
    fs.readFile('feet.txt', 'utf8', function (err, dataFeet) {
        if (err)
            reject(err)
        else
            resolve(dataFeet + '\n')
    })
})

function writeRobot(result) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', result, 'utf8',
            function (err) {
                if (err)
                    reject(err);
                else
                    resolve("Promise Success!");
            });
    });
}
let resultRobot = ""

Promise.all([readHead, readBody, readLeg, readFeet]) // promise.all เป็นตัวที่กำหนดให้ใครทำงานก่อนหลัง 
    .then(function (result) {// เอาไปรวมเป็น result is arr
        
        console.log('All completed:', result)
        for (i = 0; i < result.length; i++) {
            resultRobot += result[i] 
        }
        return writeRobot(resultRobot) //ให้นำค่านี้ออกมา
    }).catch(function (error) {//จับ errของทุกตัว
        console.error("there ' s an error", error)
    })
