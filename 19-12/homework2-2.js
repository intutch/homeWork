let fields = ['id','firstname','lastname','company','salary']; 
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];

let newJson = []

for(let i in employees) { //การใช้ for..in i ตั้งให้เป็น key ส่วนemployees คือข้อมูลที่่เรานับ arr เพราะจะนับจำนวนสมาชิกก่อน จะได้กำหนด {obj} ได้ ข้างนับ obj จเป็นข้แทูลของสมาชิก ในที่นี้มี 4 คน ทำงาน 4 รอบ
    let objInJson = {}
    for(const a in fields) { // แล้วจึงทำตัวข้างในที่จะเป็นkeyของสมาชิก (ข้อมูล ลายระเอียดต่าง ๆ) จะทำงาน 5 รอบ id fname lname company salary
        objInJson[fields[a]] = employees[i][a] 
        //console.log(objInJson)
        //newJson.push(objInJson)
    }
    newJson.push(objInJson)// push ทั้ง 5 เข้าไป ใน arr
    
}
console.log(newJson)