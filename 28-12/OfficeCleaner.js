const {Employee} = require('./employee.js')

class OfficeCleaner extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary)
        this.id = id
        this.dressCode = dressCode
    }
    work() {
        this.Clean();
        this.KillCoachroach();
        this.DecorateRoom();
        this.WelcomeGuest();
    }
    Clean(){
        console.log('clean clean')
    }
    KillCoachroach(){
        console.log('kill ittttt !!!!!')
    }
    DecorateRoom(){
        console.log('DecorateRoom')
    }
    WelcomeGuest(){
        console.log('Hello')
    }
}

exports.OfficeCleaner = OfficeCleaner