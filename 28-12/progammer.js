const {Employee} = require('./employee.js');

class progammer extends Employee {
    constructor(firstname, lastname, salary, id, type){
        super(firstname, lastname, salary)
        this.id = id
        this.type = type
    }
    work(){
        this.createWeb ()
        this.fixComputer()
        this.installWidown()
    }
    createWeb (){
        console.log(this.firstname+ " createWeb")
    }
    fixComputer(){
        console.log(this.firstname+ " fixComputer")
    }
    installWidown(){
        console.log(this.firstname+ " installWidown")
    }
}

exports.progammer = progammer 