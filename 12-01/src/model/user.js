require('../lib/db.js')()
async function getuser (){
    const db =  await Pool.getConnection()

    await db.beginTransction()
}
getuser()
class User {
    constructor(db, row) {
        this._db = db
        this.firstname = row.firstname
        this._id = row.id
        this.lastname = row.lastname
    }
    async save() {
        if (!this._id) {
            const result = await this._db.exeute(`
            insert into user (
                firstname, lastname
            ) values(
                ?,?
            )
            `, [this.firstname, this.lastname])
            this.id = result.insertID
            return
        }
        return this._db.exeute(`
        update user
        set 
            firstname = ?,
            lastname = ?,
        where id = ?
        `, [this.firstname, this.lastname, this._id])
    }
    remove() {
        return this._db.execute(`
        delete from users where id = ?
        `, [this.id])
    }
}
// let db = new User(require('../lib/db.js')())
module.exports = function (db) {
    return {
        async find(id) {
            const [rows] = await this.db.execute(`
    select
    first_name, last_name
    from users
    where id = ?
    `, [id])
            return new User(db, rows[0])
        },
        async findAll() {
            const [rows] = await this.db.execute(`
    select
    first_name, last_name
    from users
    `)
            return rows.map((row) => new User(db, row))
        }
    }
}