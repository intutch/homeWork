const mysql = require('mysql2/promise')
// module.exports = async(config) => {
//     const pool = mysql2.createPool({
//         host: 'localhost',
//         user: 'root',
//         database: 'codecamp'
//     })
// }

async function aa() {

    try {
        const pool = mysql.createPool({
            host: 'localhost',
            port: '3306',
            user: 'root',
            database: 'codecamp'
        })
        const db = await pool.getConnection()
        await db.beginTransaction()
        const [rows] = await db.execute('select * from user')
        console.log(rows)
    } catch (err) {
        console.log(err)
    }
}

aa()