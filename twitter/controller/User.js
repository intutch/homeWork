module.exports = (pool, repo) => {
    return {
        async search(ctx) {
            ctx.body = 'search'
        },
        async follow(ctx) {
            ctx.body = 'follow'
        },
        async unFollow(ctx) {
            ctx.body = 'unFollow'
        },
        async listFollow(ctx) {
            ctx.body = 'listFollow'
        },
        async listFollowed(ctx) {
            ctx.body = 'listFollowed'
        }
    }
}