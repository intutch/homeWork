module.exports = (app) => {
    app.use(Unauthorized)
}

async function Unauthorized(ctx, next) {
    if (ctx.path == '/auth/signin') {
        await next();
        return;
    }
    if (ctx.path == '/auth/signup') {
        await next();
        return;
    }
    if (!ctx.session ||
        (ctx.session && !ctx.session.logIn)
    ) {
        ctx.path = '/'
        await next();
    } else if (ctx.session.logIn) {
        await next();
    }

}