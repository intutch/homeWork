const Router = require('koa-router')
const db = require('../config/db.json')

const ctrlUser = require('../controller/User')
const ctrlUpload = require('../controller/upload')
const ctrlTweet = require('../controller/tweet')
const ctrlDiMess = require('../controller/directmessage')
const ctrlNoti = require('../controller/noti')
const ctrlAuth = require('../controller/auth')

const repoUser = require('../repo/user')
const repoTweet = require('../repo/tweet')
const repoPoll = require('../repo/poll')
const repoNoti = require('../repo/noti')

const pool = require('../lib/db')(db)

const ctrUserRepo = ctrlUser(pool, repoUser)
const ctrlTweetRepo = ctrlTweet(pool, repoTweet)
const ctrlPoll = ctrlTweet(pool, repoPoll)
const ctrlUploadImg = ctrlUpload(pool, repoUser)
const ctrlDiMessRepo = ctrlDiMess(pool, repoUser)
const ctrlNotiRepo = ctrlNoti(pool, repoNoti)
const ctrlAuthRepo = ctrlAuth(pool, repoUser)

const router = new Router()
    .get('/', ctrlAuthRepo.index)
    //auth
    .post('/auth/signup', ctrlAuthRepo.signup)
    .post('/auth/signin', ctrlAuthRepo.signin)
    .get('/auth/signout', ctrlAuthRepo.signout)
    .post('/auth/verify', ctrlAuthRepo.verify)
    //upload
    .post('/auth/upload', ctrlUploadImg.uploadImg)
    //User
    .patch('/user/:id', ctrUserRepo.search)
    .put('/user/:id/follow', ctrUserRepo.follow)
    .delete('/user/:id/follow', ctrUserRepo.unFollow)
    .get('/user/:id/follow', ctrUserRepo.listFollow)
    .get('/user/:id/followed', ctrUserRepo.listFollowed)
    //tweet
    .get('/tweet', ctrlTweetRepo.list)
    .post('/tweet', ctrlTweetRepo.create)
    .put('/tweet/:id/like', ctrlTweetRepo.like)
    .delete('/tweet/:id/like', ctrlTweetRepo.unLike)
    .post('/tweet/:id/retweet', ctrlTweetRepo.reTweet)
    .put('/tweet/:id/vote/:voteId', ctrlPoll.vote)
    .post('/tweet/:id/reply', ctrlTweetRepo.createReply)
    //noti
    .get('/notification', ctrlNotiRepo.list)
    //direct message
    .get('/message', ctrlDiMessRepo.listAll)
    .get('/message/:userId', ctrlDiMessRepo.list)
    .post('/message/:userId', ctrlDiMessRepo.sand)

module.exports = (app) => {
    app.use(router.routes())
}